GitTracker - #powdersim2 factoids, Limnoria issues linker etc.

# GitTracker

GitTracker is a customized version of ProgVal's LimnoriaChan Plugin and is meant for use on [#powdersim2] and is loaded on BigWolfy1339 there.

# Usage

LimnoriaChan has two commands called "issue" and "issuepl". Issue opens an issue to [Powder Sim 2's issue tracker] and issuepl to [issue tracker of wolfy1339's BigWolfy1339].

[#powdersim2]:irc://irc.freenode.net/#powdersim2
[Powder Sim 2's issue tracker]:https://github.com/wolfy1339/Powder-Sim2/issues
[Issue tracker of wolfy1339's BigWolfy139]:https://github.com/ProgVal/BigWolfy1339/issues

GitTracker also includes factoids and ability to get issue links. They can be get by saying e.g %%issue#123 on channel, which returns https://github.com/wolfy1339/Powder-Sim2/pull/123 , or e.g %%git , which returns the git clone URL for Powder Sim 2, git://github.com/wolfy1339/Powder-Sim2.git .
